describe('It should load the first screen', () => {
  beforeEach(async () => {
    await device.reloadReactNative();
  });

  it('should have buttons', async () => {
    await expect(element(by.id('btn_send_event'))).toBeVisible();
    await expect(element(by.id('btn_crash_app'))).toBeVisible();
  });

  it('should send an AppCenter Event', async () => {
    await element(by.id('btn_send_event')).tap()
  });
})