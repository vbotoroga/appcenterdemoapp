describe('It should open and close the modal', () => {
  // beforeEach(async () => {
  //   await device.reloadReactNative();
  // });

  it('should open the modal', async () => {
    await expect(element(by.id('btn_open_modal'))).toBeVisible();
    await element(by.id('btn_open_modal')).tap();
    await expect(element(by.text("Modal Title"))).toBeVisible();
  });

  it('should close the modal', async () => {
    await element(by.id('btn_close_modal')).tap();
    await expect(element(by.text("Modal Title"))).toBeNotVisible()
  });
})