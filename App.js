import React, { Component } from 'react';
import { StyleSheet, Text, View, Button, Modal } from 'react-native';
import codePush from "react-native-code-push";
import Analytics from 'appcenter-analytics';

class App extends Component {
  state = {
    showModal: false,
  }

  sendEvent = () => {
    Analytics.trackEvent("receivedGoods", { 
      store: "20100",
      barcodeProducts: 5,
      rfidProducts: 10,
      reAssociations: 1,
    })
  }

  crashApp = () => {
    this.fakeFunction()
  }

  checkUpdates = () => {
    codePush.sync()
  }

  toggleModal = () => this.setState({ showModal: !this.state.showModal })

  render() {
    return (
      <View style={s.container}>
        <Text style={s.welcome}>AppCenter Integration Demo</Text>

        <Button testID="btn_open_modal" title="Open Modal" onPress={this.toggleModal} />
        <Button testID="btn_send_event" title="Send AppCenter Event" onPress={this.sendEvent} />
        <Button testID="btn_crash_app" title="Force a Crash" onPress={this.crashApp} />
        {/*<Button title="Check for Updates" onPress={this.checkUpdates} color={'red'} />*/}

        <Modal animationType="slide" visible={this.state.showModal}>
          <View style={s.modal}>
            <Text>Modal Title</Text>
            <Button testID="btn_close_modal" title="close" onPress={this.toggleModal} />
          </View>
        </Modal>
      </View>
    );
  }
}

const s = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: '#fff',
  },
  welcome: {
    fontSize: 20,
    textAlign: 'center',
    margin: 10,
  },
  modal: {
    flex: 1,
    alignItems: 'center',
    justifyContent: 'center',
    backgroundColor: '#f5f5f5',
  },
});

export default codePush(App)